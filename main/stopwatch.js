// A new stopwatch
// ** this.duration
// ** start()
// ** stop()
// ** reset()

function StopWatch() {
    let duration = 0;
    let startTime = 0;
    let started = false;

    /*
    Start stopwatch and log the current time in millis to the private property
    startTime.
     */
    this.start = function () {
        // If already started, throw an error message stating so.
        if (started) {
            throw new Error('Stopwatch is already started.')
        }

        startTime = new Date().getTime();
        started = true;
    };

    /*
    Stop stopwatch then record endTime and calculate duration.
     */
    this.stop = function () {
        // If already stopped, let the user know about it with an error msg.
        if (!started) {
            throw new Error('Stopwatch not yet started.')
        }

        duration = duration + (new Date().getTime() - startTime);
        started = false;
    };

    /*
    Reset the duration to 0
     */
    this.reset = function () {
        duration = 0;
    };

    Object.defineProperty(this, 'duration', {
        get: function () {
            if (started) {
                return (duration + (new Date().getTime() - startTime)) / 1000;
            } else {
                return duration / 1000;
            }
        }
    });
}

module.exports = StopWatch;
