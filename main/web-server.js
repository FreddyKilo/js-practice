const express = require('express');
const request = require('request');

const app = express();
const lat = '33.4484';
const lon = '-112.0740';
let rise_set_url = `https://api.sunrise-sunset.org/json?lat=${lat}&lng=${lon}&formatted=0`;

app.get('/', function(req, res){
    res.send('Hello World!');
});

app.get('/sunrise', function (req, res) {
    request.get(rise_set_url, function (err, resp, body) {
        res.send(JSON.parse(body).results.sunrise);
    });
});

app.get('/sunset', function (req, res) {
    request.get(rise_set_url, function (err, resp, body) {
        res.send(JSON.parse(body).results.sunset);
    })
});

app.listen(3000);
console.log('Starting server...');