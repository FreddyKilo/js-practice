const gradebook = {
    _grades: [],

    addGrade: function (grade) {
        this._grades.push(grade);
    },

    size: function () {
        return this._grades.length;
    },

    getAverage: function () {
        let total = 0;
        for (let i = 0; i < this.size(); i++) {
            total += this._grades[i];
        }
        return total / this.size();
    },

    reset: function () {
        this._grades = [];
    }
};

exports.gradebook = gradebook;