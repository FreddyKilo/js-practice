const request = require('request');
const assert = require('assert');
const creds = require('./credentials').creds;

function NasaApiTest() {

    this.getApod = function (url, callback) {
        console.log('Executing getApod()');
        request.get(url, function (error, response, body) {
            callback(error, response, body);
        });
    };

    this.apodTest = function (err, res, body) {
        console.log('Executing apodTest()');

        if (err) {
            assert.fail(err.message);
        }

        const responseBody = JSON.parse(body);
        assert.equal(res.statusCode, 200);
        assert.equal(responseBody.media_type, 'image');
        assert.ok(responseBody.hdurl);
        assert.ok(responseBody.title);
        assert.ok(responseBody.explanation);
        assert.ok(responseBody.date);
    };
}

let nasa = new NasaApiTest();
nasa.getApod('https://api.nasa.gov/planetary/apod?api_key=' + creds.nasaApiKey, nasa.apodTest);

console.log('-----');
