const book = require('../main/gradebook').gradebook;

exports['setUp'] = function (callback) {
    console.log('Running setUp()...');
    book.reset();
    callback();
};

exports['Testing gradebook count'] = function (test) {
    book.addGrade(100);
    book.addGrade(100);
    book.addGrade(100);
    let count = book.size();

    test.equal(count, 3);
    test.done();
};

exports['Testing gradebook average'] = function (test) {
    book.addGrade(100);
    book.addGrade(50);

    test.equal(book.getAverage(), 75);
    test.done();
};