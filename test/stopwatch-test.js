// Import the Stopwatch constructor function (see the export in stopwatch.js)
const Stopwatch = require('../main/stopwatch');
const assert = require('assert');

let stopwatch;

const wait = function (millis) {
    const startTime = new Date().getTime();
    while(startTime + millis > new Date().getTime()){}
};

exports['setUp'] = function (callback) {
    stopwatch = new Stopwatch();
    callback();
};

exports['Testing duration before stopping'] = function (assert) {
    stopwatch.start();
    wait(3000);
    let duration = stopwatch.duration;
    assert.ok(duration >= 3.0);
    assert.ok(duration <= 3.1);
    stopwatch.stop();
    assert.done();
};

exports['Testing duration after stopping'] = function (assert) {
    stopwatch.start();
    wait(2500);
    stopwatch.stop();-
    assert.ok(stopwatch.duration >= 2.50);
    assert.ok(stopwatch.duration <= 2.51);
    assert.done();
};
